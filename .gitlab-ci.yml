---
variables:
  KOJI_TAG: 'linuxsupport'
  BUILD_7: 'False'
  # Match it with the specfile Release
  DIST_8s: '.el8s.cern'
  BUILD_8s: 'True'
  BUILD_9: 'False'

# DO NOT MODIFY THIS
include: 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/rpm-ci.yml'
# DO NOT MODIFY THIS

.test_rpmlint:
  allow_failure: true

.rpm_deps:
  before_script:
    # Needed to allow yum-builddep to find our sources during the dep resolution, as it parses the specfile anyway
    - yum-builddep -y *.spec --define "_topdir $(pwd)/build" --define "_sourcedir $(pwd)/src"
    # configure: error: no acceptable C compiler found in $PATH
    - yum install gcc glibc glibc-common gd gd-devel -y

# Make sure we can't tag this rpm by accident, to avoid it being installed anywhere
# Although it should not break anything, we do not want this rpm to be used in CERN's nodes
# We want to take the built rpm to produce the required bootloader binaries, that's it

.koji_build:
  except:
    - tags

.tag_qa:
  except:
    - tags

.tag_stable:
  except:
    - tags

test_install8s_x86_64:
  extends: test_install8s
  dependencies:
    - koji_scratch8s
  script:
    - grub2-mkimage -c ./grub-embedded_x86_64.cfg -p '(tftp)/blah' -o grubx64-with-embed.efi -O x86_64-efi efinet echo configfile net efinet tftp gzio part_gpt all_video efifwsetup linux loadenv ls http gfxmenu chain png serial linux loopback udf sleep gfxterm png gfxterm_background minicmd terminal test
    # Ref. https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/tree/qa/code/files/aimsloaders/aims/config/uefi/themes
    # Normally you do not need to update the fonts nor theme, but just in case. We have no menu for ARM64 (yet?)
    - yum install -y dejavu-fonts-common dejavu-sans-fonts dejavu-sans-mono-fonts
    - grub2-mkfont -s 14 -o Sans-14.pf2 /usr/share/fonts/dejavu/DejaVuSansMono-Bold.ttf
    - grub2-mkfont -s 16 -o Sans-16.pf2 /usr/share/fonts/dejavu/DejaVuSansMono-Bold.ttf
    # Artifact paths cannot be outside build dir. Workaround https://gitlab.com/gitlab-org/gitlab-foss/-/issues/15530#note_265790866
    - mkdir -p $CI_PROJECT_DIR/grub2_x86_64-efi_binaries
    - cp -r /usr/lib/grub/x86_64-efi/*.mod $CI_PROJECT_DIR/grub2_x86_64-efi_binaries/
    - cp -r /usr/lib/grub/x86_64-efi/*.lst $CI_PROJECT_DIR/grub2_x86_64-efi_binaries/
    - cp -r /usr/lib/grub/x86_64-efi/*.sh $CI_PROJECT_DIR/grub2_x86_64-efi_binaries/
    - cp grubx64-with-embed.efi $CI_PROJECT_DIR/grub2_x86_64-efi_binaries/
    - mkdir -p $CI_PROJECT_DIR/theme_fonts && cp -r *.pf2 $CI_PROJECT_DIR/theme_fonts/
  artifacts:
    paths:
      - theme_fonts/
      - grub2_x86_64-efi_binaries/
    expire_in: 1 month
  only:
    variables:
      - $SKIP_KOJI_DOWNLOAD == 'False' && $BUILD_8s == 'True'

test_install8s_aarch64:
  extends: test_install8s
  dependencies:
    - koji_scratch8s
  script:
    - grub2-mkimage -c ./grub-embedded_arm64.cfg -p '(tftp)/blah' -o grubarm64-with-embed.efi -O arm64-efi efinet echo configfile net efinet tftp gzio part_gpt all_video efifwsetup linux loadenv ls http gfxmenu chain png serial linux loopback udf sleep gfxterm png gfxterm_background minicmd terminal test
    # Artifact paths cannot be outside build dir. Workaround https://gitlab.com/gitlab-org/gitlab-foss/-/issues/15530#note_265790866
    - mkdir -p $CI_PROJECT_DIR/grub2_arm64-efi_binaries
    - cp -r /usr/lib/grub/arm64-efi/*.mod $CI_PROJECT_DIR/grub2_arm64-efi_binaries/
    - cp -r /usr/lib/grub/arm64-efi/*.lst $CI_PROJECT_DIR/grub2_arm64-efi_binaries/
    - cp -r /usr/lib/grub/arm64-efi/*.sh $CI_PROJECT_DIR/grub2_arm64-efi_binaries/
    - cp grubarm64-with-embed.efi $CI_PROJECT_DIR/grub2_arm64-efi_binaries/
  artifacts:
    paths:
      - grub2_arm64-efi_binaries/
    expire_in: 1 month
  only:
    variables:
      - $SKIP_KOJI_DOWNLOAD == 'False' && $BUILD_8s == 'True'
  # Ref. https://gitlab.cern.ch/gitlabci-examples/gpu-tpu-arm-public-clouds/-/blob/master/.gitlab-ci.yml
  tags:
    - docker-arm

# Avoid running this as we already run two jobs, one per arch
test_install8s:
  only:
    variables:
      - $NEVER == 'True'
