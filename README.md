# GRUB2

This project has the instructions for building up a new version of GRUB2 with the required patches for AIMS2 loaders:

* <https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/tree/qa/code/files/aimsloaders/aims/loader/x86_64-efi>
* <https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/tree/qa/code/files/aimsloaders/aims/loader/arm64-efi>
* <https://gitlab.cern.ch/ai/it-puppet-hostgroup-aims/-/tree/qa/code/files/aimsloaders/hwreg/loader/x86_64-efi>

Now properly automated with [RPMCI](https://gitlab.cern.ch/linuxsupport/rpmci).

## Considerations

* `Release` on SpecFile has an increased number.
* `src/grub.patches` has `Patch1000: 1000-sleep-extra-keys.patch`, which is also included on the `src/` dir.
    * This patch was built by using `git diff` on the corresponding <https://github.com/rhboot/grub2> branch. Check which RedHat version your CentOS Stream 8 rpm corresponds to
* Force the legacy building on x86_64 to avoid rpm dependency problems, check `with_legacy_modules` value:

```
%ifarch x86_64
%global with_efi_common 1
# Originally was %global with_legacy_modules 0
%global with_legacy_modules 1
%global with_legacy_common 0
%else
%global with_efi_common 0
%global with_legacy_common 1
%global with_legacy_modules 1
%endif
```

## Upgrade

Normally all you need to do is to take the previous considerations, update the `src/` content with the src rpm you can get upstream, and if needed update `src/1000-sleep-extra-keys.patch`.
